__author__ = 'Viktor Hundar V'

'''In advance sorry for any comments in russian'''

'''
The Game!
для подготовки к написанию "игры" подготовьте класс Unit. под подготовкой я подразумеваю продумать структуру, атрибуты, 
методы, возможные взаимодействия между обьектами, параметры инстанцирования и т.д.

у базового юнита должны быть:
идентификатор (имя?)
здоровье (численное значение)
атака (численное значение)
защита (численное значение)
возможность атаковать другого юнита (метод)
возможность защищаться от другого атаки другого юнита (метод) ?

Comments:
48-62 - в данной конфигурации механики вычисления урона - согласен. но когда в вычислениях размера участвуют еще 10-15 
статических значений + 10-15 генерируемых системой - станет немного сложнее.

к этому добавить какието доп действия при атаке (типа повреждение инвентаря или 5-и секундное кровотечение и т.д.) - 
получим лютый ужас в коде. потому я и пинаю вас, чтоб вы все помимо основной логики ничего левого не вносили. 
ты уже уклонение в отдельную функцию вынес (подозреваю, чтобы не усложнять код лишним) - отличная идея. 
еще одна - все вычисления по количеству урона за один раз - тоже в отдельную функцию. еще одна - убрать декларации 
функций на уровень класса в приватные методы (вдруг для разных типов юнитов, расс, профессий или хзчего вычисления 
понадобятся разные - сможем для них перегрузить)

Next task:
The Game - Unit
У вас уже есть Unit. Реализуйте следующее:
возможность добавлять юниту оружие и доспехи. Это потребует отдельного класса отвечающего за объект амуниции. 
Одевание на юнита обьекта амуниции производит пересчет характеристик юнита ()

создайте класс Битва, который принимает два юнита и реализует механику битвы - юниты поочередно атакуют друг-друга 
пока у одного из них не здоровье не станет 0
*так модифицируйте метод атака у юнита, чтобы юнит наносил неодинаковый урон при каждом ударе. 
напимер randint(x,y) где x и y - показатели минимальной и максимальной атаки юнита (как создать или задать мин и макс 
показатели атаки - на ваше усмотрение).
ну и как обычно - чем чище, понятнее и лаконичнее код - тем чучше

код хорош, спорить не стану.
в одной умной книге было написано - код должен выглядеть так, чтоб было понятно , что над ним потрудились. 
этот очень похож! респект!
- неплохо бы продумать место для установки снаряжения, а то у тебя получается что юниту можно штук 20 мечей в руки 
насовать и еще десяток сапогов одеть)) + продумай отдельный метод для просмотра текущего экипа и метод для замены экипа.
125 - у тебя не дуэль получилась, а массовая драка )) причем с интересным выбором противников. если уже так - тогда 
сделай отдельно толпа на толпу, отдельно дуэль

а в целом оч хорошо
'''

from random import randint
from random import choice

class Unit():
    def __init__(self, name: str, hp=100, att=15, def_=3):
        self.name = name    # unit's name
        self.hp = hp    # unit's health points
        self.hp_max = hp        # maximum hp value,to whcih it will be restored after battle
        self.att = att  # unit's attack points
        self.def_ = def_    # unit's defence points
        self.inventory = []     # unit's inventory
        self._inventory_limit = 6        # unit's maximum amount of items in inventory
        self.initstats = [hp, att, def_]    # initial stats for respawn

        # print(f'Моё имя \'{self.name}\', у меня {self.hp} здоровья, я наношу до {self.att} атаки, и у меня защита в {self.def_} единицы')
        print(f'My name is {self.name}, my health is {self.hp}, I deal {self.att} damage, my armor is {self.def_}')

    def attack(self, attacked):
        '''
        Function of attacking attacked object
        :param attacked: Object, which is attacked
        :return: True if attack occurs, False if attacked object evades the attack
        '''
        if not isinstance(attacked, Unit):      # for now, units can attack only obj of Unit class
            raise TypeError
        if self.hp <= 0: return     # in case attacker has low hp, he cannot attack any more
        attack = randint(int(self.att*0.8), int(self.att))  # each attack value will be between 80% and 100% of initial value

        if self._evasion(attacked):
            # print(f'{attacked.name} смог уклониться от устрашающих {attack} урона от {self.name}')
            print(f'{attacked.name} could evade scaring {attack} damage from {self.name}')
            return False

        attacked.hp -= self._do_damage(attacked, attack)
        return True

    def _evasion(self, obj):
        '''
         evasion function is a propability to avoid the attack, depends on the amount of hp
         100hp give 0.12*100hp = 12% chance to avoid attack
        :return: True or False
        '''
        chance = int(0.12 * obj.hp)  # [%] - chance in percents
        chance_array = range(0, chance,
                             1)  # chance_array is given as a list of numbers which contains {chance} amount of numbers
        rand_value = randint(0, 100)  # total 100% probability represented by a set of numbers from 0 to 100
        return True if rand_value in chance_array else False

    def _do_damage(self, obj, attack_value):
        '''
        Calculates amount of damage caused to attacked object (obj)
        :param obj: Object which is attacked
        :param attack_value: Amount of attack from self object
        :return: Amount of damage
        '''
        self.damage = attack_value - obj.def_  # damage value caused to attacked unit
        self.damage = self.damage if self.damage >= 0 else 0  # to prevent healing of the attacked unit
        return self.damage

    def respawn(self):
        '''
        Function that respawns hero, in order to not create him once again. Sets all stats to the ones set in __init__
        '''
        self.hp = self.initstats[0]  # unit's health points
        self.att = self.initstats[1] # unit's attack points
        self.def_ = self.initstats[2]  # unit's defence points
        self.inventory = []  # unit's empty inventory


    def take_to_inventory(self, *args):
        '''
        Updates unit's characteristics adding ammos' ones
        '''
        if not all([isinstance(i, Ammo) for i in args]): raise TypeError

        def check_ammo_type(ammo_object):
            '''
            Checks, if Unit can pick up one more ammo of some type
            :param ammo_object: Ammunition object to pick up
            :return: True if amount of ammo class in inventory is less than limit value of ammo, else - None
            '''
            count = 0
            for obj in self.inventory:
                if ammo_object.__class__ == obj.__class__:
                    count += 1
            if count < ammo_object.limit:
                return True

        for i in args:
            if not check_ammo_type(i):
                print(f'{self.name} cannot take one more {i.name}')
                continue
            if len(self.inventory) < self._inventory_limit:
                self.inventory.append(i)
                # self.inventory.append(f"{i.name}({i.type})")
            else:
                print(f"{self.name} cannot take any item no more")
                break
            self.hp += i.hp
            self.hp_max += i.hp
            self.att += i.att
            self.def_ += i.def_
            # print(f'\'{self.name}\' взял {i.name}. Теперь у \'{self.name}\', {self.hp} здоровья, до {self.att} атаки, и {self.def_} единицы защиты')
            print(f'{self.name} took {self._report_item(i)}')
            self._report_stats()

    def _report_item(self, object):
        '''
        Creates a string representing name of ammo and its characteristics
        :param object: Object of Ammo
        :return: String with its name and influence to characteristics
        '''
        input_dict = {"hp": object.hp, "damage": object.att, "armor": object.def_}
        show_list = []
        for attribute in input_dict:
            if input_dict[attribute] > 0:
                show_list.append(f"+{input_dict[attribute]} {attribute}")
            elif input_dict[attribute] < 0:
                show_list.append(f"{input_dict[attribute]} {attribute}")
        return f'{object.name}({", ".join(show_list)})'

    def _report_stats(self):
        return print(f"Now {self.name} has {self.hp} hp, causes up to {self.att} damage and has {self.def_} armor")

    def rmv_from_inventory(self, *args):
        '''
        Updates unit's characteristics when remmoving item
        '''
        if not all([isinstance(i, Ammo) for i in args]): raise TypeError
        for i in args:
            try:
                self.inventory.remove(i)
                self.hp -= i.hp
                self.hp_max -= i.hp
                self.att -= i.att
                self.def_ -= i.def_
                print(f"{self.name} removed {i.name} {self._report_item(i)}")
                self._report_stats()
            except ValueError as e:
                print(f"There's no {i.name} to remove")

    def show_inventory(self):
        return print(f'{self.name} has in his inventory: {", ".join([f"{item.name}({item.type})" for item in self.inventory])}')

    def _heal(self):
        print(f'{self.name} has restored his hp from {self.hp} up to {self.hp_max}')
        self.hp = self.hp_max
        return


'''
1. Возможность добавлять юниту оружие и доспехи. Это потребует отдельного класса отвечающего за объект амуниции.
Одевание на юнита обьекта амуниции производит пересчет характеристик юнита ()
'''
class Ammo():
    type = "" # ammo's type
    limit = 1  # maximum amount per unit
    def __init__(self, name, hp=0, att=0, def_=0):
        self.name = name  # ammo's name
        self.hp = hp  # ammo's health points
        self.att = att  # ammo's attack points
        self.def_ = def_  # ammo's defence points

'''
неплохо бы продумать место для установки снаряжения, а то у тебя получается что юниту можно штук 20 мечей в руки 
насовать и еще десяток сапогов одеть)) 
+ продумай отдельный метод для просмотра текущего экипа.
'''
class ColdSteelArms(Ammo):
    type = "Cold Steel Arms"
    limit = 2

class HeavyColdSteelArms(Ammo):
    type = "Heavy Cold Steel Arms"

class Shield(Ammo):
    type = "Armor"

class HeadShield(Shield):
    type = "Head Armor"

class ChestShield(Shield):
    type = "Chest Armor"

class HandShield(Shield):
    type = "Hand Armor"

class LegShield(Shield):
    type = "Leg Armor"

class Stamina(Ammo):
    type = "Source of Stamina"
    limit = 5

'''
2. Создайте класс Битва, который принимает два юнита и реализует механику битвы - юниты поочередно атакуют друг-друга,
пока у одного из них не здоровье не станет 0
'''

class Battle():
    def __init__(self, *args): # def __init__(self, unit1: Unit, unit2: Unit):
        if  len(args) < 2 and len(args) % 2 != 0:
            raise ValueError('Battle object should have even number of objects Unit class, minimum 2')
        if not all([isinstance(unit_, Unit) for unit_ in args]):
            raise TypeError
        self.units = args
        return

    def _fight_couple(self, couple, respawn):
        '''
        Proceeds with a fight between two units
        '''
        for i in couple:
            i._report_stats()
            i.show_inventory()
        a, b = couple
        self._try_luck(a)
        self._try_luck(b)
        while all([unit_.hp > 0 for unit_ in couple]):
            if a.attack(b):
                if b.hp > 0:
                    # print(f'{a.name} нанёс {b.name} {a.damage} урона',
                    #       f'\nУ {b.name}-а осталось {b.hp} здоровья')
                    print(f'{a.name} caused {b.name} {a.damage} damage',
                          f'\n{b.name} has {b.hp} hp left')
                else:
                    # print(f'{a.name} нанёс критические {a.damage} урона', f'\n{b.name} пал замертво')
                    # print(f'{a.name} выиграл битву с оставшимися {a.hp} здоровья')
                    print(f'{a.name} caused critical {a.damage} damage', f'\n{b.name} fell dead...')
                    print(f'{a.name} won a battle with remaining {a.hp} hp')
                    print(f'{a.name} - winner!')
                    a._heal()
                    if respawn:
                        b.respawn()
                        # print(f'=========================', f'\n{b.name} вернулся из царствия Аида',
                        #       '\n=========================')
                        print(f'=========================', f'\n{b.name} has resurrected',
                              '\n=========================')
                    return
            a, b = b, a

    def fight(self, respawn=False):
        '''
        Proceeds with a fight between all the units dividing them by couples
        '''
        try:
            for unit_n in range(len(self.units)):
                if unit_n % 2 != 0: # to go for next couple, which does not include previous unit
                    continue
                couple = (self.units[unit_n], self.units[unit_n+1])     # creating couples for fighting
                self._fight_couple(couple, respawn)
                unit_n += 2
        except:
            pass

    def _try_luck(self, obj):
        '''
        Randomly cahnges either attack or armor of an obj
        :param obj: obj, which will be subjected to "luck"
        :return: None
        '''
        luck_range = [i - 5 for i in range(11)]
        attr_range = ["armor", "damage"]
        delta = choice(luck_range)
        attr = choice(attr_range)
        if attr == "armor":
            obj.def_ += delta
        elif attr == "damage":
            obj.att += delta
        _str = str(delta) if delta < 0 else f"+{delta}"
        print(f"{obj.name} IS LUCKY! He has boost {_str} {attr}", )
        obj._report_stats()


# ==========================================================================
# ==========================================================================

dwarf1 = Unit('Kuvalda the mighty hammer', 70, 20, 2)
elf1 = Unit('Lacalut the white teeth', 85, 15, 1)
huntress1 = Unit('Swarzkopf the silky hair', 90, 17, 2)
huntress2 = Unit('Palmolive the gentle gel', 95, 13, 1)

sword = ColdSteelArms("Punisher", att=9)
scimitar = ColdSteelArms("Yataghan", att=8)
hammer = HeavyColdSteelArms('Puncher', att=12)
spear = HeavyColdSteelArms('Holepuncher', att=8)
helmet = HeadShield('Keeper of the Head', def_=3)
chestarmor = ChestShield('SaveMyHeart', def_=10)
legarmor = LegShield('Terminator boots', def_=4)
shield = Shield("Defender", def_=4, att=-1)
heart = Stamina("Tarrasque", hp=30, def_=1)
heal1 = Stamina("Heal", hp=15)

# Battle(dwarf1, elf1).fight(True)

Battle(dwarf1, elf1, huntress1, huntress2).fight(True)

dwarf1.take_to_inventory(hammer, shield, helmet, chestarmor, chestarmor)
dwarf1.rmv_from_inventory(helmet)
dwarf1.rmv_from_inventory(sword)
elf1.take_to_inventory(sword, scimitar, heal1, heart, helmet, legarmor)

Battle(dwarf1, elf1).fight()